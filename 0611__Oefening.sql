USE ModernWays;

SELECT Games.Titel, COALESCE (Platformen.Naam, 'Geen platformen gekend') AS 'Naam'
FROM Releases
INNER JOIN Platformen ON Platformen_Id = Platformen.Id
RIGHT JOIN Games ON Games.Id = Games_Id
WHERE Releases.Platformen_Id IS NULL

UNION ALL

SELECT COALESCE (Games.Titel, 'Geen games gekend'), Platformen.Naam
FROM Releases
INNER JOIN Games ON Games.Id = Games_Id
RIGHT JOIN Platformen ON Platformen_Id = Platformen.Id
WHERE Releases.Platformen_Id IS NULL