USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleasesLoop`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleasesLoop` (IN extraReleases INT)
BEGIN
	DECLARE counter INT DEFAULT 0;
    DECLARE success BOOL;
    
    callLoop: loop
        CALL MockAlbumReleaseWithSuccess(success);
        if success = 1 then 
			set counter = counter + 1;
		end if;
        if counter = extraReleases then
			leave callLoop;
		end if;
	end loop;
END$$
DELIMITER ;