USE aptunes;
-- select count(distinct left(Voornaam,9)) from Muzikanten
CREATE INDEX VoornaamFamilienaamIdx
ON Muzikanten(Voornaam(9),Familienaam(9));