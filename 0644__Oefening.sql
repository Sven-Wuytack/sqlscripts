USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumRelease`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumRelease` ()
BEGIN
	DECLARE numberOfAlbums INT DEFAULT 0;
    DECLARE numberOfBands INT DEFAULT 0;
    DECLARE randomAlbumId INT DEFAULT 0;
    DECLARE randomBandId INT DEFAULT 0;
    
    SELECT COUNT(*)
    INTO numberOfAlbums
    FROM Albums;
    
    SELECT COUNT(*)
    INTO numberOfBands
    FROM Bands;
    
    SET randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
    SET randomBandId = FLOOR(RAND() * numberOfBands) + 1;
    
    IF  (randomBandId, randomAlbumId) not in (select * from Albumreleases)
        THEN
		INSERT INTO AlbumReleases (
        Bands_Id,
        Albums_Id
        )
        VALUES (
        randomBandId,
        randomAlbumId
        );
	END IF;
END$$
DELIMITER ;