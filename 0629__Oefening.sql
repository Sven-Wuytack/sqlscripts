use ModernWays;

select Studenten_Id as 'Id'
from Evaluaties
group by Studenten_Id
having avg(Cijfer) > (select avg(Cijfer) from Evaluaties);

-- OUDE OPLOSSING:
-- use ModernWays;
-- select Id from Studenten
-- inner join Evaluaties
-- on Evaluaties.Studenten_Id = Studenten.Id
-- group by Studenten.Id
-- having avg(Cijfer) > (select avg(Cijfer) from Evaluaties);