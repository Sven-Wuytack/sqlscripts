USE `aptunes`;
DROP procedure IF EXISTS `CleanupOldMemberships`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CleanupOldMemberships` (IN endDate DATE, OUT num INT)
BEGIN
start transaction;
select count(*) into num FROM Lidmaatschappen
WHERE Lidmaatschappen.Einddatum is not null and Lidmaatschappen.Einddatum < endDate;
SET SQL_SAFE_UPDATES = 0;
DELETE FROM Lidmaatschappen
WHERE Lidmaatschappen.Einddatum is not null and Lidmaatschappen.Einddatum < endDate;
SET SQL_SAFE_UPDATES = 1;
commit;
END$$
DELIMITER ;