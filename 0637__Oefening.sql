-- creer een aparte nieuwe tabel persoon (algemene tabel) met info
-- over de personen die we nadien kunnen gebruiken in andere tabellen door
-- gebruik te maken van foreign keys. 
use ModernWays;
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);

-- Nu zorgen we ervoor dat de voornamen en familienamen van in Boeken in de tabel
-- personen worden gezet met een subquery. We zorgen met de 'select distinct' ervoor dat we hier geen
-- dubbele waarden gaan hebben.
insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;
   
-- tabel Personen uitbreiden met extra informatie over de personen
alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);
   
-- door volgende select-script uit te voeren kan je controleren dat er geen dubbele
-- gegevens in zitten en dat alle kolommen in de tabel Personen zit:

-- select * from Personen order by Familienaam, Voornaam;

-- foreign key om te verwijzen naar de auteur toevoegen
-- verwijzing zou beter not null zijn
-- op dit moment niet mogelijk, want de kolom moet ingevuld worden
-- op basis van vergelijking tussen Boeken en Personen
alter table Boeken add Personen_Id int null;

-- Om de Id van Personen te kopiëren in de tabel Boeken moeten we eerst een relatie
-- leggen tussen de twee tabellen. Welke kolommen zijn in beide tabellen gelijk?
-- De kolom Voornaam en Familienaam want we hebben ze net gekopieerd.
-- We kunnen bekijken hoe we de twee tabellen zullen linken op basis van deze twee kolommen:

-- select Boeken.Voornaam,
--    Boeken.Familienaam,
--    Boeken.Personen_Id,
--    Personen.Voornaam,
--    Personen.Familienaam,
--    Personen.Id
-- from Boeken cross join Personen
-- where Boeken.Voornaam = Personen.Voornaam and
--     Boeken.Familienaam = Personen.Familienaam;

-- kopieer in de geldige combinaties Boeken/Personen
-- het ID van de persoon
-- naar de verwijzing in een Boek naar een Persoon
-- m.a.w. zorg ervoor dat een Boek verwijst naar zijn auteur
SET SQL_SAFE_UPDATES = 0;
update Boeken cross join Personen
set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
Boeken.Familienaam = Personen.Familienaam;
SET SQL_SAFE_UPDATES = 1;

-- Nu mogen ID's verplicht not null worden
-- foreign key verplicht maken
alter table Boeken change Personen_Id Personen_Id int not null;

-- We testen of we hebben wat we wilden:
-- select Voornaam, Familienaam, Personen_Id from Boeken;

-- overbodige kolommen mogen weg
alter table Boeken drop column Voornaam,
drop column Familienaam;

-- Nu kunnen we de kolom Personen_Id "promoveren" tot een echte foreign key kolom:
-- dan de constraint toevoegen
alter table Boeken add constraint fk_Boeken_Personen
foreign key(Personen_Id) references Personen(Id);




    
