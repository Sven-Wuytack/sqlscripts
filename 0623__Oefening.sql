-- toont alle geldige combinaties van liedjestitels en genres
-- zelfde opmerking als boven
-- je mag een index op meerdere kolommen samen maken

CREATE INDEX NaamIdx ON Genres(Naam);

select Titel, Naam
from Liedjesgenres inner join Liedjes
on Liedjesgenres.Liedjes_Id = Liedjes.Id
inner join Genres
on Liedjesgenres.Genres_Id = Genres.Id
where Naam = 'Rock';