USE ModernWays;

SELECT AuteursBoeken.Auteur, AuteursBoeken.Titel, AVG(gemiddelderatings.Rating)
FROM AuteursBoeken
INNER JOIN gemiddelderatings
ON AuteursBoeken.Titel = gemiddelderatings.Titel;