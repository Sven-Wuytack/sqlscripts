CREATE USER if not exists student IDENTIFIED BY 'ikbeneenstudent';
GRANT EXECUTE ON PROCEDURE aptunes.GetAlbumDuration TO student;
GRANT EXECUTE ON PROCEDURE aptunes.GetAlbumDuration2 TO student;
GRANT SELECT ON TABLE aptunes.Liedjes TO student;