USE ModernWays;

CREATE VIEW GemiddeldeRatings
AS
SELECT AuteursBoeken.Auteur, AuteursBoeken.Titel, AVG(Reviews.Rating)
FROM AuteursBoeken
INNER JOIN Reviews
ON AuteursBoeken.Boeken_Id = Reviews.Boeken_Id
GROUP BY AuteursBoeken.Titel;

