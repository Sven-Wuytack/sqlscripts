USE ModernWays;
ALTER TABLE Baasjes
ADD COLUMN Naam_Id INT,
ADD CONSTRAINT fk_Baasjes_Huisdieren
  FOREIGN KEY (Huisdieren_Id)
  REFERENCES Huisdieren(Id);