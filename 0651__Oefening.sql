USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetAlbumDuration` (IN album INT UNSIGNED, OUT totalDuration SMALLINT UNSIGNED)
BEGIN
	DECLARE songDuration TINYINT UNSIGNED default 0;
    DECLARE ok bool default false;
	DECLARE songDurationCursor CURSOR FOR SELECT Lengte FROM Liedjes WHERE Albums_Id = album;
    DECLARE continue handler for not found set ok = True;
	set totalDuration = 0;
    open songDurationCursor;
	fetchloop: LOOP
		FETCH songDurationCursor INTO songDuration;
        -- volgende drie regels meteen na de fetch, direct nadat er niks is gevonden direct de lus verlaten en dan kom je bij de 'close songDurationCursor'
        if ok = True then
			leave fetchloop;
        end if;
		SET totalDuration = totalDuration + songDuration;
	END LOOP fetchloop;
    close songDurationCursor;
END$$
DELIMITER ;