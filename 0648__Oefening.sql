USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DemonstrateHandlerOrder` ()
BEGIN
DECLARE randomValue tinyint default 0;
DECLARE CONTINUE HANDLER FOR SQLSTATE '45002'
begin
	select 'State 45002 opgevangen. Geen probleem.';
end;
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION 
begin
	select 'Een algemene fout opgevangen.';
end;
set randomValue = floor(rand() * 3) + 1;
if randomValue = 1 then
	SIGNAL SQLSTATE '45001';
ELSEIF randomValue = 2 THEN
   SIGNAL SQLSTATE '45002';
ELSE
   SIGNAL SQLSTATE '45003';
end if;
END$$
DELIMITER ;