USE ModernWays;

SELECT Games.Titel, Platformen.Naam
FROM Releases
LEFT JOIN Games ON Releases.Games_Id = Games.Id
RIGHT JOIN Platformen ON Releases.Platformen_Id = Platformen.Id
ORDER BY Games.Titel;