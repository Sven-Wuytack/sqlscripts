USE `aptunes`;
DROP procedure IF EXISTS `CreateAndReleaseAlbum`;

delimiter $$

use `aptunes`$$
CREATE PROCEDURE `CreateAndReleaseAlbum` (IN titel VARCHAR(100), IN bands_Id INT)
BEGIN
  start transaction;
  insert into Albums (Titel)
  values (titel);
  -- hier mogelijk dat iemand anders een andere instructie runt daarom start transaction
  insert into AlbumReleases (Bands_Id, Albums_Id)
  values (bands_Id, LAST_INSERT_ID());
  commit;
END$$

delimiter ;