use ModernWays;
select Voornaam, Familienaam
from Directieleden
-- gebruik juiste tabel in subquery! Geen tabel Directieleden maar tabel Personeelsleden
where Loon < ANY (select Loon from Personeelsleden);