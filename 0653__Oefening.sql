USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration2`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetAlbumDuration2` (IN albumId INT UNSIGNED, OUT totalDuration SMALLINT UNSIGNED)
sql security invoker

BEGIN
  DECLARE songDuration TINYINT UNSIGNED default 0;
  DECLARE currentSong
  CURSOR FOR SELECT Lengte FROM Liedjes WHERE Albums_Id = albumId;

  OPEN currentSong;

  getSong: LOOP
	FETCH currentSong INTO songDuration;
    SET totalDuration = totalDuration + songDuration;
    END LOOP getSong;

  CLOSE currentSong;
END$$
DELIMITER ;