USE ModernWays;

CREATE VIEW AuteursBoeken
AS
SELECT CONCAT(Personen.Voornaam, ' ', Personen.Familienaam) AS Auteur, Boeken.Titel
FROM Boeken 
INNER JOIN Publicaties
ON
Boeken_Id = Boeken.Id
INNER JOIN Personen
ON
Personen_Id = Personen.Id;