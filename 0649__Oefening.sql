USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DemonstrateHandlerOrder` ()
BEGIN
DECLARE randomValue tinyint default 0;

DECLARE EXIT HANDLER FOR SQLEXCEPTION
begin
	RESIGNAL SET MESSAGE_TEXT = 'Ik heb mijn best gedaan!';
end;

DECLARE CONTINUE HANDLER FOR SQLSTATE '45002'
BEGIN
	RESIGNAL SET MESSAGE_TEXT = 'State 45002 opgevangen. Geen probleem.';
END;
    
set randomValue = FLOOR(RAND() * 3) + 1;
if randomValue = 1 then
	SIGNAL SQLSTATE '45001';
ELSEIF randomValue = 2 THEN
   SIGNAL SQLSTATE '45002';
ELSE
   SIGNAL SQLSTATE '45003';
end if;
END$$
DELIMITER ;