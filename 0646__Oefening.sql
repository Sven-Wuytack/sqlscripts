USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleases`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleases` (IN extraReleases INT)
BEGIN
	DECLARE counter INT DEFAULT 0;
    DECLARE success BOOL;
    
    REPEAT
        CALL MockAlbumReleaseWithSuccess(success);
        if success = 1 then 
			set counter = counter + 1;
		end if;
    UNTIL counter = extraReleases
    END REPEAT;
END$$
DELIMITER ;