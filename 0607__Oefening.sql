USE ModernWays;

SELECT Leden.Voornaam, Taken.Omschrijving
FROM Taken
LEFT JOIN Leden ON Taken.Leden_Id = Leden.Id
WHERE Taken.Leden_Id IS NULL;