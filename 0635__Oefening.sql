use ModernWays;
select Leeftijdscategorie, avg(Loon) as 'Gemiddeld loon'
from 
(select Id, floor(Leeftijd/10)*10 as Leeftijdscategorie
from Personeelsleden) as Leeftijdscategorieen
inner join Personeelsleden
on Leeftijdscategorie = Personeelsleden.Id
group by Leeftijdscategorie
order by Leeftijdscategorie;
