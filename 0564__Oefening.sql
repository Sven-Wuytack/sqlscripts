USE ModernWays;
CREATE TABLE Studenten (
Studentennummer INT AUTO_INCREMENT PRIMARY KEY,
Voornaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Familienaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL
);
CREATE TABLE Opleidingen (
Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL PRIMARY KEY
);
CREATE TABLE Lectoren (
Voornaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Familienaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Personeelsnummer INT AUTO_INCREMENT PRIMARY KEY
);
CREATE TABLE Vakken (
Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL PRIMARY KEY
);
ALTER TABLE Studenten
/*deze kolom wordt verplicht omdat er een volle lijn te zien is:*/
ADD COLUMN Opleidingen_Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
/*Semester bij kant van student omdat hier de n staat, de n naar 1*/
ADD COLUMN Semester TINYINT UNSIGNED NOT NULL,
/*een foreign key is een beperking, er moet een verwijzing zijn:*/
ADD CONSTRAINT fk_Studenten_Opleidingen
FOREIGN KEY (Opleidingen_Naam)
/*Maar bestaat dat vak wel? daarom references*/
REFERENCES Opleidingen(Naam);
/*inschrijving hoef je niet aan te maken aangezien dit een N op 1 relatie is
als er een m op n relatie is moet je van die rechthoeken (Opleidingsonderdeel en Geeft) 
een tabel maken. de tabel hieronder kon evengoed ook VakGeeftLector noemen*/
CREATE TABLE LectorGeeftVak (
Lectoren_Personeelsnummer INT NOT NULL,
Vakken_Naam VARCHAR(100) CHAR SET utf8mb4,
CONSTRAINT fk_LectorGeeftVak_Lectoren
FOREIGN KEY (Lectoren_Personeelsnummer)
REFERENCES Lectoren(Personeelsnummer),

CONSTRAINT fk_LectorGeeftVak_Vakken
FOREIGN KEY (Vakken_Naam)
REFERENCES Vakken(Naam)
);

CREATE TABLE VakOpleidingsonderdeelOpleiding (

);

